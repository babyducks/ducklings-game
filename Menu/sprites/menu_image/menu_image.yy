{
    "id": "38666889-b865-4f2c-a294-b863c8251b3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menu_image",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e17b502-f53a-4415-8fa3-c4391a52d218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38666889-b865-4f2c-a294-b863c8251b3f",
            "compositeImage": {
                "id": "cc1308bb-2239-440e-ad6b-4aee2448620f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e17b502-f53a-4415-8fa3-c4391a52d218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba8a1c26-ec0f-4162-96eb-d17fa05fa160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e17b502-f53a-4415-8fa3-c4391a52d218",
                    "LayerId": "0284c9ed-87a0-41f6-b044-75dd57b5c7da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 964,
    "layers": [
        {
            "id": "0284c9ed-87a0-41f6-b044-75dd57b5c7da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38666889-b865-4f2c-a294-b863c8251b3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 964,
    "xorig": 0,
    "yorig": 0
}