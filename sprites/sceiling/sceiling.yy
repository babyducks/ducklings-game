{
    "id": "f9c89af3-c6db-442d-8e7a-ef767def7ca1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sceiling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92192f9c-2fd2-40a2-9e93-397572e0bc1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9c89af3-c6db-442d-8e7a-ef767def7ca1",
            "compositeImage": {
                "id": "eb2f2637-da66-4e13-a970-08c808bf504c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92192f9c-2fd2-40a2-9e93-397572e0bc1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd18598-6dcd-4be9-897f-48ac4091686b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92192f9c-2fd2-40a2-9e93-397572e0bc1c",
                    "LayerId": "91454fc2-1baa-4f44-957f-2f439cf9c489"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "91454fc2-1baa-4f44-957f-2f439cf9c489",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9c89af3-c6db-442d-8e7a-ef767def7ca1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 17
}