{
    "id": "599b0a03-a20e-43fe-b170-2af96b397e2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 19,
    "bbox_right": 138,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c67b4834-0a81-4e5a-aa0e-be18db900a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599b0a03-a20e-43fe-b170-2af96b397e2a",
            "compositeImage": {
                "id": "01c9b504-2ae2-47bf-92d5-0e331e0b7b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c67b4834-0a81-4e5a-aa0e-be18db900a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2467a2c4-258a-40ac-b9d0-d07cbe5a4e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c67b4834-0a81-4e5a-aa0e-be18db900a87",
                    "LayerId": "ebad00cc-6a06-401f-872f-332f455c9886"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "ebad00cc-6a06-401f-872f-332f455c9886",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "599b0a03-a20e-43fe-b170-2af96b397e2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 164,
    "xorig": 117,
    "yorig": 67
}