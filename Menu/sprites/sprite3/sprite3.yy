{
    "id": "5bdb1f59-63ab-4d37-af0a-670a0262ceb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "281c9a90-8cbb-4b71-9692-f049d9fd21c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bdb1f59-63ab-4d37-af0a-670a0262ceb1",
            "compositeImage": {
                "id": "50ad7540-e29e-40a3-9b9a-5cd7f0ca79a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "281c9a90-8cbb-4b71-9692-f049d9fd21c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13b4bcb-62b6-48e0-bd61-f1a3ac61c453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "281c9a90-8cbb-4b71-9692-f049d9fd21c7",
                    "LayerId": "ee8aa8a5-7347-426a-81f3-65e2bf768a54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "ee8aa8a5-7347-426a-81f3-65e2bf768a54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bdb1f59-63ab-4d37-af0a-670a0262ceb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}