{
    "id": "e7a61e57-59ee-409f-8123-43d5d60f0666",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Jumping",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "694023cc-d988-4f5b-9553-8bc31a9267ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7a61e57-59ee-409f-8123-43d5d60f0666",
            "compositeImage": {
                "id": "4c41d896-70ef-4fe8-b149-54fb482e364c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "694023cc-d988-4f5b-9553-8bc31a9267ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e16451-48d9-4a7a-872a-b4847b0fe2cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "694023cc-d988-4f5b-9553-8bc31a9267ec",
                    "LayerId": "148993d0-1268-45b7-a2c9-cfc7c1b12b5b"
                }
            ]
        },
        {
            "id": "5e294a14-8c89-4123-a2c5-23a2a61d4b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7a61e57-59ee-409f-8123-43d5d60f0666",
            "compositeImage": {
                "id": "a0ac9aa0-de76-4af5-9902-64da68d063c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e294a14-8c89-4123-a2c5-23a2a61d4b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b413e52-372d-4d36-8b28-bd43b15300f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e294a14-8c89-4123-a2c5-23a2a61d4b60",
                    "LayerId": "148993d0-1268-45b7-a2c9-cfc7c1b12b5b"
                }
            ]
        },
        {
            "id": "f050f7be-a3ba-4616-b0b4-6f916d2827c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7a61e57-59ee-409f-8123-43d5d60f0666",
            "compositeImage": {
                "id": "03154e02-a25c-4870-9aaf-14d8a53742e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f050f7be-a3ba-4616-b0b4-6f916d2827c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a273eaaf-2f79-4618-b96b-234d6df62372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f050f7be-a3ba-4616-b0b4-6f916d2827c8",
                    "LayerId": "148993d0-1268-45b7-a2c9-cfc7c1b12b5b"
                }
            ]
        },
        {
            "id": "6bf0f031-2976-4845-ac59-9e072e184a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7a61e57-59ee-409f-8123-43d5d60f0666",
            "compositeImage": {
                "id": "77e465a4-4bff-4a9f-8a35-42d515181845",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf0f031-2976-4845-ac59-9e072e184a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e7ba5a0-6c75-42a0-b77b-86a95e1f5546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf0f031-2976-4845-ac59-9e072e184a65",
                    "LayerId": "148993d0-1268-45b7-a2c9-cfc7c1b12b5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "148993d0-1268-45b7-a2c9-cfc7c1b12b5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7a61e57-59ee-409f-8123-43d5d60f0666",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 22
}