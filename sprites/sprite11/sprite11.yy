{
    "id": "1faf4577-fb35-415a-a605-cfa95cbe0124",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bace28e5-7f17-4a62-b712-ef2c73f71076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1faf4577-fb35-415a-a605-cfa95cbe0124",
            "compositeImage": {
                "id": "f8063da2-c10c-4046-b6d0-56a99daec294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bace28e5-7f17-4a62-b712-ef2c73f71076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b1bc628-b0d5-485f-af8a-cdab288423ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bace28e5-7f17-4a62-b712-ef2c73f71076",
                    "LayerId": "375ee386-b33b-40c4-b0c6-f2a14300450d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "375ee386-b33b-40c4-b0c6-f2a14300450d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1faf4577-fb35-415a-a605-cfa95cbe0124",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}