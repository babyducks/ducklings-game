{
    "id": "eddca9ec-62bf-4c0b-9ed9-c8893b8a2bce",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bell MT",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "633d7d90-6b60-42fa-adb1-5befdab444d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ab5091fd-11e7-4020-9381-70e9a47a06bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 143,
                "y": 78
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c3d20d89-597d-43f1-8c21-94157ccad643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 132,
                "y": 78
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f0a52a32-0e22-4b47-8d54-87917b988f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 111,
                "y": 78
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "17a74f9e-c209-439b-ac6e-868ecef0475c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 95,
                "y": 78
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bd917a65-1ed3-423d-b6da-1fc41dd26b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 71,
                "y": 78
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "79abdc97-5a80-4f1c-ba84-f9648bbde982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 48,
                "y": 78
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3cca361b-05f5-4bd6-953c-6e31e4d97dee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 42,
                "y": 78
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "368806b6-ccd4-47f4-b5c1-1c31d480a00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 78
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f0ef3587-b099-4d07-9a24-01020de4f033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "146c1e4b-957f-4314-b5d5-1c8c90fa7c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 150,
                "y": 78
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "092b284c-5004-410d-bf14-9aebece2a057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "25bc34fd-20a1-415a-9217-1693a92988a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 477,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "71c29279-452f-48bd-934c-dbd174d17f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 464,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f022cc86-411e-4d7e-8bf8-d1746bf26af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 457,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f2292d23-2945-4438-becc-f3486a8bba3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 441,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a54d3f6b-236f-43d7-8663-446ab9c06383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 423,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1bc69918-25ee-401e-a071-278ab235536f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 4,
                "shift": 16,
                "w": 9,
                "x": 412,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9ced6007-51d8-4a99-baf4-2b331aad8338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 396,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f4f9ce8f-81bf-4682-a622-1decdc6ad183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 381,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "011eda05-cbd1-47e4-bf9d-6515e396d674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 363,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "48639063-53ca-49d9-a9e2-f758e4aa5543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 484,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e2798f7f-771e-4d7f-907a-1e88736f43e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 166,
                "y": 78
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ba323a6e-c324-46bb-8180-50ec70b7a155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 182,
                "y": 78
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3a9ee881-232d-4b0c-a307-885a365aad98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 197,
                "y": 78
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d220a576-d0c2-4a96-9c53-d7ffefa8e280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 119,
                "y": 116
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f91ac4c0-6dda-46ec-a29e-eebb1ec0c4a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 112,
                "y": 116
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3dfa6fc2-2b9c-4fe5-bce8-6a31625724dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 105,
                "y": 116
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4303df91-f9fb-4326-a6ed-aa7bdd8255a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 85,
                "y": 116
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "25f8d173-ef2d-4b15-a0a2-a2823d5497e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 65,
                "y": 116
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "146b8d57-3743-4aed-815c-1f9ba6c4a655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 45,
                "y": 116
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f3696256-9e12-489e-b5a8-21a4e0f391af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 32,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cf9c0e59-bf9a-4bf9-b4ba-26a27ccd9414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a6de5ada-3346-40a5-9a7b-268adf86f7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 477,
                "y": 78
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f35abe38-9535-476d-b117-2c6037e05a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 457,
                "y": 78
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "282a2442-1c4f-4607-b640-6ea48db82995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 435,
                "y": 78
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8be52052-2669-4d46-9285-9651042aa904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 410,
                "y": 78
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "aeab8d1f-124d-4465-9cb0-1fe54119c19d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 388,
                "y": 78
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9e422826-82a6-4081-8fb1-83b6c83904be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b28552ba-b0ff-4042-a6e4-53de70bbb641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 341,
                "y": 78
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7c0e445a-16ce-41bc-9213-bd500bb9f78c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 315,
                "y": 78
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "be8013e9-c557-459d-bb83-a3fa713257a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 303,
                "y": 78
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "411babe9-4780-4dff-a814-fe6c90b2eeb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 289,
                "y": 78
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b2a77fbb-feab-4532-b93b-afac1ea23506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 266,
                "y": 78
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "edda24cf-76cb-449e-b7c7-7817869e403d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 244,
                "y": 78
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d20de5a8-5b66-4c05-94a3-edd7ea4493ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 212,
                "y": 78
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3e58a52b-088a-4e40-9f76-ff80ff0176e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 337,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "049575b8-5e21-4cf2-b9a4-9a383c495e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 314,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dd050f14-f0aa-4020-8b15-6fa67c1c32da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 294,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "51adc06a-345c-4a0d-9b57-a7856f12e138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 416,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "41467b0d-0d82-4153-aa4a-8e93adc4a35c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 378,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fe5dfdaa-bc5b-4382-8cbe-75db96bf2a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ae64c126-517d-431d-a55a-e1933ee05dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "838eb700-8934-413f-af7c-b36376a1234a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "df1c841e-6e7a-4561-bf35-e7808b3c3d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e2e51ade-ebe8-46e5-b11e-855f385af1f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "013e9e58-7e45-48d3-8cab-7d227823a267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "509adbd4-c12b-4076-b6fc-58fe83d54f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "90ba0525-0543-4aa8-9bf9-feb03258379b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f7b4e41d-b7c7-4bf8-8394-d7925ead7e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 3,
                "shift": 16,
                "w": 13,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a40007ac-d8ca-4a7c-a254-83285695b31c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "728c88e6-69ed-4a95-95ee-640cc4fc818b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d0ac0645-958a-473e-8dec-6aecad123353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8ee99664-fc42-41b4-a99e-f89d72c91a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ea33fbc3-a675-4820-91db-e265df69f0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "166e0a88-d3ea-49be-9885-063af72c6b76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1c4fd78f-bd4f-4532-a060-24dcdf870003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f02c0f44-36c1-44d6-9167-4d02693c8f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "25e307cb-9835-4ac8-913d-68d3c7b5e723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "49fd69ff-cd64-4ffb-b5f3-9317ba61c5d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d5f4541c-b02b-4fbd-a03f-3570cce1f87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 13,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d36dd53a-382f-4a88-a79d-b72af1b83e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f2d0f5c3-43e1-494d-b6b4-30e40e9a97f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 99,
                "y": 40
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1c4eb8a3-133f-47a7-96dc-6b81c5397965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 458,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fc46174c-da8c-4b5d-b3c9-307c456b0d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": -3,
                "shift": 9,
                "w": 9,
                "x": 270,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "092a7027-3f09-44d0-9785-0f6a7ec66fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 252,
                "y": 40
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b24d022e-4ce4-4c03-b64f-04e8b170b0b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 241,
                "y": 40
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b23f2ef6-d58c-4d98-b555-3095445f8a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 214,
                "y": 40
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ba3fc8d6-4110-49a9-b945-e9a23365fbfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 196,
                "y": 40
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "375350b8-1ad8-476b-a68c-4a9b6dd2cc0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 40
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5d665eaf-f8ac-4211-ae4f-39e0c03172da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 162,
                "y": 40
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "adc986a2-d79f-4645-8212-361e4e198b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 144,
                "y": 40
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7e76bdc8-f9a0-4e20-8e71-0fb7c4200b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 129,
                "y": 40
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ae82271f-ee1a-44a5-b9ba-3efdd77f6b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 281,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bb073961-653c-4c49-80b4-d588f7468055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 117,
                "y": 40
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f97fc99e-3dfb-4125-8840-79172d1cee98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 82,
                "y": 40
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b45cdb7b-510a-406f-a305-d9fb59b5a52d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a5b6dbe8-dae6-4ceb-b6a6-8729ea46b98b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 39,
                "y": 40
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e11d3605-9aa1-4281-b844-4415264224c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 20,
                "y": 40
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f57201e4-6267-4e47-b5b3-6e73ebe48cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f8d40568-59d6-4fdb-b45d-aeb19b8ae72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 495,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d0104ee4-4a11-425e-9361-298a0045eb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 4,
                "shift": 15,
                "w": 10,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0792cb33-e260-4a18-94bc-0de86938eeb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 7,
                "shift": 16,
                "w": 2,
                "x": 479,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c2e3da30-28fe-435d-8699-6c975c4b1f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 467,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "72de0da2-3d58-4a81-addd-5aecd2944d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 135,
                "y": 116
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "48bb7b35-7e25-4d84-809d-58c8dfaebe55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 36,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 154,
                "y": 116
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a0a16a98-a97b-4fb2-ad1c-383d7da8d84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 67
        },
        {
            "id": "784c0d50-5b7e-4e30-a288-8a79e7d0d8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 71
        },
        {
            "id": "b4f16a1d-962c-465f-a97e-36f594e8ec4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 79
        },
        {
            "id": "c4226ed0-973f-4dcb-ad53-77e8514659a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 81
        },
        {
            "id": "e6c0ef15-87f5-4ea5-bff4-f5f8af9d3caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "0eb200ef-150d-4ec7-9466-5eed378ec9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "1683b8fe-44df-4e15-9690-fe4eeacd0cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "1fbab92f-e09f-4508-9fd7-a309c015230c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "c90bb627-185d-4047-ac1b-f8557cba0f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "916b2e6a-4cbb-483a-8402-b19de48b2a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "ae532390-28c0-422d-a0b9-9c85dc0a2996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 119
        },
        {
            "id": "5559d84a-65ba-4ed9-8a3d-5f2844a3b81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "7d8e515b-feb4-4ff6-9b4e-17829dee6017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "6edbcb4a-972f-4478-9b86-549495d75254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8221
        },
        {
            "id": "51ea518c-8a61-4c14-9b09-28e1fe5817c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "953702ec-200a-4a64-8599-fd3bd2dd82e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 220
        },
        {
            "id": "25872a36-2203-479d-917e-a9e323bcd20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 46
        },
        {
            "id": "ec378359-fde8-4a23-bd71-8b9863383bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "52dd4c3e-b016-45b2-8521-fe28fda16efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "54138b92-9a77-47b5-9785-c0c12f03edf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "f118c47c-a50e-4f42-9f54-95b11aff45e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "c8bbaebc-f46a-4928-aca6-ee797875ed6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "13d72bb0-4614-457f-a711-2529b499c049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "e6e8a98d-ceca-4668-9542-152153d5a571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 196
        },
        {
            "id": "c75e2253-aadc-458d-a7a5-c72276ea5ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 197
        },
        {
            "id": "d91517ed-1e68-46c2-906e-efd55fdf14dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "26e1abfc-e716-4a8a-91e0-54da02fb309e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "5768861a-b3f3-4fa0-a728-6b465e9a6d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "0a1f7f97-537f-4640-b17d-b3078d2394e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "01ef00bf-40f9-4745-92e6-7e6c6d51808d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "cf2da2fa-3c3d-4c44-9d33-5e437f0c8efc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 108
        },
        {
            "id": "710a783a-eb84-48c0-98c5-71e2f3c725d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "5ba5c095-38a0-407a-865c-06fc9163e2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 196
        },
        {
            "id": "73e3d19e-134a-4afa-8cc6-efbde1c68a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 197
        },
        {
            "id": "3b2615a4-e74f-40c2-8923-354dd4323d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 228
        },
        {
            "id": "1145877e-988a-4d0d-a6fa-f61818dbcd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 229
        },
        {
            "id": "297f0c3c-12a8-465c-8765-2562a9c386cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 246
        },
        {
            "id": "1e39ae41-4936-4ebc-b76e-97b8799b9372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 44
        },
        {
            "id": "678452d5-65e8-48cd-b1e0-8ccf28b6325e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 46
        },
        {
            "id": "a997c82b-442e-4da0-aa24-d4bcd075f4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 44
        },
        {
            "id": "6d796ba6-3ba5-44b6-8636-4e3e6ba48fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 46
        },
        {
            "id": "b5f89c6b-196e-4b9e-a538-7e7d08a7c65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "a29760c4-2345-4195-ab0f-ebd6b5dc088e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "009024b4-8741-40e0-a2c2-2bf7267eda03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "354a72f8-185b-40c3-a5a7-c389bb19317f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 97
        },
        {
            "id": "6a3cd6c4-e168-4320-a83d-632c227c39a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 101
        },
        {
            "id": "13404ded-ebec-48a0-b45d-10f6df474d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 111
        },
        {
            "id": "b55f1ea5-a4bc-4f5e-8687-7c226e674e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 117
        },
        {
            "id": "10f0e7f9-92c7-4f08-aa76-93e42a9f87c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 121
        },
        {
            "id": "6e91453a-d555-4fa9-848a-42a55aca6d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "41f80a64-d88c-4aa1-8122-1d109873843b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "47528ccf-9d28-4e63-9ccd-586dbf8ca367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 228
        },
        {
            "id": "547a4a8d-9e96-4354-b84e-238dcc0b50ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 229
        },
        {
            "id": "5b748921-885d-4ea1-a884-a7e40f5d13d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 246
        },
        {
            "id": "b71653c5-87b1-449d-ac7d-9cb13ee101dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 252
        },
        {
            "id": "05ed0c72-d600-480b-8819-55ee653bc18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "357cb9be-06d8-424f-8e9c-444e5602cdc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "9f09f07b-d81f-4e03-9c38-2df76eb47b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "b17a3e3a-5ed2-4adc-9024-f0869624d85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "d84dec95-b112-4a2a-a508-5200b161425a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "a00878d4-1aab-437e-ab8e-7f382b1b8df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "ccadd9a8-e755-4ac2-ae54-bcadfe7a81f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "f2ddc7e6-c534-4aa8-b343-5eaa901d05e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "fa4ec3bc-ef80-4399-b2b0-dbc62a9eeb77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "ff118d3c-e8fb-4966-afb1-584363570bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "857e5cd7-bd97-44f8-aeb3-60df702d55ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "290331fb-38ea-4478-becd-45e90ce9e668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "5d553bdf-d9f6-477b-b285-707eac0a6bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "2e7c3291-91b9-479a-8b9a-759b072b2702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "0faf2e16-9271-46e2-92d1-371082038f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "8d181f3f-a317-457d-9948-81a51f66a0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "9ae26391-61e3-44d3-814f-3318c7170dd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "72a35650-de1f-447b-822a-c60158082a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "88c1ae8b-6fc6-4365-8fd9-1054f4590fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "afa2b347-ee2d-423a-9c47-84ea94e26c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "f955fb9f-5cb3-4a11-ae22-10c2874b8275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 46
        },
        {
            "id": "e8c861bb-65ed-495e-a434-a6a2322b1e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 65
        },
        {
            "id": "18bd2b8c-2a99-4338-941f-bd604c50685c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 196
        },
        {
            "id": "71fc09ca-2b17-4904-bc3b-c85eadb118be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 197
        },
        {
            "id": "bf0664eb-0989-4335-a60b-9a2377bc1ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 44
        },
        {
            "id": "9e9a5103-36f6-4277-8424-d82aa0ee81a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 46
        },
        {
            "id": "e9fd5947-dc01-4648-9a67-ee361c1a3fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 65
        },
        {
            "id": "ac4100df-9d87-47b0-9897-d786a76315fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "21510ad0-47cc-4eda-8170-418f30f797ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 87
        },
        {
            "id": "b150f769-86df-41e6-b926-cfaa71e6a360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "1e295c9c-22f7-4d97-bf1a-a69a51c0d561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 89
        },
        {
            "id": "c3af6a12-a328-4d44-9f89-16eea6ef222b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 196
        },
        {
            "id": "c40e513e-343f-41ef-94cf-01d259f31afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 197
        },
        {
            "id": "85dcd914-1a17-458e-92b9-8b86212f33d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "e08c5758-d79a-462a-9f02-e2e6a35cec93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "b7f6816e-f19a-40f9-a86e-5db13e42f9f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "4b680217-4f38-4b7e-8cad-f503ef3d412f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "11c750bb-ee1c-4b07-acda-d4fd18a634df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "940d61d8-44ae-480d-bbda-d3eef9fe63bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "7230d863-5253-4258-a27b-5d4423290f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 196
        },
        {
            "id": "0ad7c742-f6ad-4928-b673-76aa17014b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 197
        },
        {
            "id": "b573ee21-65ce-4b22-8df1-3e51ff863480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "357727db-86a4-4d3c-913c-90d5c1546a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 46
        },
        {
            "id": "d26b4acf-646a-4ec5-ab5d-452bbe7d9af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 65
        },
        {
            "id": "d853f9f5-2aff-4d9b-89b3-4e1ff741f2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "52c5fd31-7288-4d47-8710-87b1e8281664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 85
        },
        {
            "id": "1cd85e7d-4241-4172-aaf2-fb34b00e60dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 86
        },
        {
            "id": "3768b667-b43e-4eb5-b6b0-b374f413a68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 87
        },
        {
            "id": "5598f623-4cc6-43f6-a408-676c3045dbb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 89
        },
        {
            "id": "f0d8a9af-d50f-43a4-a61d-ff3c4e56a372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "ef03ba39-6201-45c5-aa20-c958f5a1fe24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "00f57284-6fc6-4d2c-8455-034e1c514f6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "b1d371db-62ec-4095-b455-5cf8ae33fb37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "07a41fd6-6922-45c5-97bc-7e3cc55d987b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "426c7921-b227-424a-b464-63448f2a24db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "ec786131-a986-4207-8f83-827a573facbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "964dab43-00b8-4714-9c57-ac43967ec909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "4d1cc452-d348-4364-9b54-770fe500abaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "a40440fa-40a8-477b-9034-97d3ad627397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "ed24163e-5714-4b8f-9689-9e603bfe0f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "2ba91fb1-94c1-43f0-bebb-ca2dd9f1434e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "c7f48b13-21dd-4851-9916-b19c6db98510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "fcfef1c0-b360-44c7-b30f-ac1a1dcc72fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "75602c9d-3e3c-46b2-ac1b-026558ed2da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "ddd97dfe-caa5-4e37-9d6e-ecadaaf0b7f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 214
        },
        {
            "id": "e963299f-03fe-4c41-8f95-add66932b0a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 220
        },
        {
            "id": "ee9e40f2-0972-4e55-85fa-b53a2ec9ddd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "909c4e97-efe4-45a5-bc4f-d4a6ef1d84bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 252
        },
        {
            "id": "31d12c4b-456e-48f8-b081-d0ae69dca508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "ace10294-905f-48ef-a85a-2d1bc1f54b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "ae8a328f-9dee-4b32-9d86-e255fd887222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "7f51680f-7af4-4467-adc2-827809d41e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "11ffad80-5b0b-445e-8b95-cb293447a572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "5be24ac7-7fd5-45ea-a883-7d6081941126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "592c275a-ab88-4859-8f3e-9acc3d2eab52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "a860b0a5-8632-4bc4-9c9f-de3ebeb2eb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "5ea0949b-1189-40e0-8612-60034d9ac4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "024989b8-713d-4cb2-99ca-27c2f6184210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "a38612f1-0e83-49fa-b0d1-f3d80d66cc9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "ede8d94e-0208-4f4b-ac48-1b2dfe639972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "37087866-abfc-49af-80f2-9f150b43c72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "9301c9a6-0bf4-460d-96de-97f8191a5cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "f7340a32-1269-4576-a527-fde8e1aeb8a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "13160e8f-7530-4859-b800-f7be20626824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "c46904e5-eafe-4836-afb0-461ef0322025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "ee29c7c5-862b-4099-ad44-8d0e384cd218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 44
        },
        {
            "id": "ad9713f1-09fc-4847-bf5e-a42768dd68a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 46
        },
        {
            "id": "7368c1ba-fb6b-41f7-be97-e7dd8a5faf72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 65
        },
        {
            "id": "91e75e87-9677-4dc9-992b-7b349aa6def1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 196
        },
        {
            "id": "f02de50c-d0d7-4598-8ed9-e8b1cd3b8a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 197
        },
        {
            "id": "07a9ba96-d1ce-4f58-a7db-c14ba0530b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 44
        },
        {
            "id": "52a8e8ca-9214-40e3-a132-6aa477b216b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 45
        },
        {
            "id": "7ee958f7-2c6f-458d-97c3-27d345d30573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "19d4826e-ecc1-45ed-bbae-0be0932efbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 58
        },
        {
            "id": "a1b07b55-6902-4129-a33f-9283c584dfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 59
        },
        {
            "id": "801b2c1e-b14a-4214-8be5-4f1dca28a981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "e5396e69-419f-45c7-8930-2d7b4d649fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "46b2f342-3f33-48bf-86d9-7bd80fbff890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "bb9c4cff-a589-42a9-9c88-4b2db1e354ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 79
        },
        {
            "id": "ef614739-41e3-473b-b8e0-568416c3fbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "cd74a9c4-fc48-4a8c-98e8-31483158c5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "5b78310d-bac0-42a3-b09d-bd68b234afd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "4aab9c00-5ac8-4eb1-9657-d763bca79bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "6c849044-171b-4411-80d7-22421be4bf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "956e3857-4095-4355-8479-99ef3543b0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "a0145cc1-8939-43ec-9632-2b953bad7bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "b9e7807c-513c-49e9-bde5-a59aa9ad35fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 173
        },
        {
            "id": "84b0bb0c-a4af-4489-ae67-21c29ef03028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "f8ad70e2-9dfa-4e7f-9c74-7f873d758c98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "5e34f414-2969-4a42-a567-d45fc210bfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 214
        },
        {
            "id": "03e35320-e942-4d5e-8337-345952918358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "195d8782-1719-4563-aeb5-c88978995b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 44
        },
        {
            "id": "315e0df9-c96d-4204-8196-62314cea57e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 45
        },
        {
            "id": "91f92353-d201-424f-a0e8-942abae98d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 46
        },
        {
            "id": "75ef7806-86ed-4dbc-ba7a-707b14383d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 58
        },
        {
            "id": "f47ebeda-13bf-4421-9b24-5dbba3aba3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 59
        },
        {
            "id": "f256f3fc-3cf9-4772-9b89-73bd7cd8325f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "68b80bda-2b1b-46f8-abc0-435748358314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 67
        },
        {
            "id": "7c0082e8-ff4e-45bc-988f-4cf7bc37574b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 71
        },
        {
            "id": "81002c00-6577-4968-a361-5dc3b9a594ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 79
        },
        {
            "id": "84ca8507-210a-4caa-8bb9-31abffb0e41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "b1f38a2b-4fd5-41b8-8766-844665d458b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 100
        },
        {
            "id": "a1ee18f1-6c5d-44b6-940a-ec0cd15435d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "a189881d-bcbf-4451-ac36-6ea0523fb77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 104
        },
        {
            "id": "32872a4b-a0bd-4898-8e00-ea3844ef1099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "88614e0b-c9ae-4d97-baf4-fe0acc26a0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 114
        },
        {
            "id": "52bb3bbc-bf02-4af8-9d3e-a3b68a5fdb5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 117
        },
        {
            "id": "240d983d-a837-49e3-8974-63e21ba3fbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 121
        },
        {
            "id": "76daef72-ee8f-4085-9443-5782de3ed6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 173
        },
        {
            "id": "f0d07835-1bf1-4584-afaa-3eb995ed9d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 196
        },
        {
            "id": "f9459b61-8cca-4abb-ae59-bd02cced7f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 197
        },
        {
            "id": "902ee71f-c398-4e39-b57b-e90f3ca1482b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 214
        },
        {
            "id": "c06e842d-1d45-4e90-add0-4d18601d93f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 229
        },
        {
            "id": "501306ae-a480-4d26-bfb9-cbcdbfbbb331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "815dd951-17ab-48e9-8f15-fb7c79689986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "7734cd00-8c90-419e-bd92-790fa4d2b0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "f374281b-5ed6-40d3-8710-c53eee1a6e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 214
        },
        {
            "id": "9351fca8-456c-4163-a721-0c8a2e46e492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "3e408079-ad74-4536-8d04-bdbe7bd19e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "9a016573-9789-4577-9501-1a4183537f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "8ba2fa5b-1b3f-43cf-8d9d-acd9c71a03fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "276b3b5c-24be-44f0-bd0c-f2d9902c458b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "6fa8c4bc-ba92-4b9f-8060-dbdfec40518f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "ba4966ae-f86a-4135-aec0-6b5b1667c21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "43fdf924-7b35-4834-b831-52290c59ecb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "16e5cc71-34d8-4538-9e6b-d32c5d0b9a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "0423a88c-101b-4a67-9196-539a9177531b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "e41c04d3-b36e-4ded-933d-b8319f5c27a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 100
        },
        {
            "id": "2e04acfd-1c0b-4fbb-bde6-49fd0f5cb367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "c531a5e4-8d73-4ac7-ae81-de5dc74b41a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "609a4429-fe02-427b-8c43-2e12ce5e4602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "80c8d7f4-24e3-4d3f-92bb-75fd10239eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "a8fc4377-d16b-4ac0-a35c-2671d16d2477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "e80928b4-20e0-459b-8a5d-baf23371fc4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "4f2e01e7-3310-49a4-b61d-286a19c3507e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "64c85c7f-15ab-4c8e-ba80-9486c9846888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "971d9e29-edc7-4a1e-be9f-c7d9c97d03cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "76ef2909-173e-414f-8122-21dfb1447f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "36cea5c0-4b0c-412c-a38e-476299397855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "7df8d4fb-5790-4884-a032-b1f36d58ec7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "cdf2e3ea-c004-4bf7-89ab-3d5bcf0c5be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "788d1532-ab6c-47e7-9064-6430d3b6e0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "f1b6ac22-5b17-4fd4-80ed-b3254b3d149a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "187839db-5624-4078-b365-e444735af83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "6830f874-d126-45c2-bfe0-1910f9e1f60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 46
        },
        {
            "id": "5e88aff1-1fb7-4118-9499-3150cdb373f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "df75de50-025f-4cba-ac4f-e66488ff7dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "b1cf0e56-23fa-4fe1-8f31-9197a8ee5fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 44
        },
        {
            "id": "1f9a836e-0e98-41c1-b8a7-368a686d93f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 46
        },
        {
            "id": "b7b0d250-b826-4c9c-9d40-bc6126cf652d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 101,
            "second": 120
        },
        {
            "id": "73a9e823-6030-4592-af36-cb1a72a2364b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 101,
            "second": 122
        },
        {
            "id": "3646e765-443e-4801-bc48-0ae4d69d850e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 7,
            "first": 102,
            "second": 32
        },
        {
            "id": "7eb973c1-7641-4f02-b49f-296809205c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 33
        },
        {
            "id": "90eaf2b7-99e0-4fbd-8b2c-2e86a46a89eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 63
        },
        {
            "id": "2f67d720-1d83-4ee0-87c7-4f472778367c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 107
        },
        {
            "id": "93182467-8121-4a0c-b380-fa88040ca314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 108
        },
        {
            "id": "3c50d4f4-704b-4333-b49d-a59b1e02f675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8217
        },
        {
            "id": "9ce450ae-9c02-4676-9ed7-23b105ed0645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8221
        },
        {
            "id": "a22c05cd-4839-4eb9-b771-9a11f0cddbfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 44
        },
        {
            "id": "9066ebfb-1f90-4d23-95f5-15614e28cc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 46
        },
        {
            "id": "187ac8ad-8713-4fce-a329-f56c4c62666c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 97
        },
        {
            "id": "df5087b2-7219-4488-8a20-30df7af341a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 114
        },
        {
            "id": "bed9648e-9e8b-4981-9170-da8fdb056caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 121
        },
        {
            "id": "81a224b3-2601-4013-b175-2975ceb4efb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 228
        },
        {
            "id": "08f626da-1550-4b0a-988a-289bf3960ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 229
        },
        {
            "id": "f5214c50-ae92-4fa2-8a46-da43eeb7662d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "5ba8160f-4af7-41e9-9bf4-d8097890c8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 111
        },
        {
            "id": "e742f343-6e61-4795-818e-d365dfdba94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 246
        },
        {
            "id": "921ead33-5676-4a78-ac72-3a9e6eba9235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "c4b6776f-25af-4032-ac13-d6b4b8a6e6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "37b9a0fd-51bb-4186-afb0-fd8a8f9facf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 44
        },
        {
            "id": "d761d5c7-359b-430f-88cd-51facd70001b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 46
        },
        {
            "id": "1c37383b-b87d-4330-9ed4-b147bf2c3c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "0850cc85-90bc-4a62-b731-01c13fb3b3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "a62ff664-7f79-48d0-9652-8811baa2aeea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "7a6a1242-53f2-4f03-a9fa-e836ac586a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 46
        },
        {
            "id": "716d3bfc-088e-448d-97ea-09b712a1f9e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "ce822a66-d48b-4bc1-86f1-e1606777630f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "9819d109-53c6-44ae-a9ec-e71be364dae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "5360d50f-d42d-4547-82ae-b52ea6854feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "12ee651a-aea1-4747-9dc9-5dee8f808f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 109
        },
        {
            "id": "c9c4110a-e1d8-44a8-87ee-2d911fcc4876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 110
        },
        {
            "id": "71e757e8-25cf-4e2f-bc11-2560df585010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "c0f1c4c4-9ff6-4243-a50d-99c23e049e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "8ef804db-295e-4425-ba5f-5606f4b88021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "a22c757b-3c03-4384-b033-e66a861348ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 44
        },
        {
            "id": "5b90884c-6b2b-4b7f-9cbb-78fea0bb7909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 46
        },
        {
            "id": "321b7789-2f5a-4cea-8798-789995e8078a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "549bfcc5-2ea9-4a9d-8076-7e79cae4df49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "fd769693-a3d5-41f8-bb5a-4e0cff42290a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "9a136575-5aa7-4e2b-a3c3-19415e2d2e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 99
        },
        {
            "id": "401a1a46-f636-4e40-aa84-1242cc73c4cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "d3bd34e7-1ca1-4970-83ef-7ebd13841390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "0647017d-fd5c-41ca-9570-d48e1680a301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "47b3b2d1-ff60-4e0c-9fcb-e049e140fa50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "0e511646-073c-4b0d-8a34-506242edd898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 246
        },
        {
            "id": "61406dd6-78cd-40ef-b543-c3ca02cd65ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "d16979fb-0787-4c8a-87e9-748d67200695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "7b7e9377-1f42-47ff-9ef4-1e09f260bec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 99
        },
        {
            "id": "47d30a60-644a-417e-b226-bf0ff2f27e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "20454827-0b9d-484f-a162-e03b0d072e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "c16d1ab4-6444-4476-9c10-7f8d8a435a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "cc578052-fe45-42a1-9e12-321ecd952381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "66b3aa59-b5e9-4020-b946-66c7efa315cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "ace7f702-31f2-4e3f-b7c4-b66824d53e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        },
        {
            "id": "fc0fe576-f6b9-470f-8a32-5faeefeb60ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 97
        },
        {
            "id": "41e2ff2f-8767-448c-a9cd-def2c506cd96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 99
        },
        {
            "id": "1064e646-528d-4a02-b02f-11eb6d4e35fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 100
        },
        {
            "id": "21a22d54-25e9-4c18-95e4-576735f53ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 101
        },
        {
            "id": "3bb4e459-b65b-4311-b143-c74572caecf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "94121716-3d06-4da4-95c9-38e997820684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 111
        },
        {
            "id": "2f1c1291-9132-4ee8-a5d0-321bdebb3b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 228
        },
        {
            "id": "a02752d2-23db-433b-95c4-a2889556b363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 229
        },
        {
            "id": "37156194-3550-4cbb-9ac0-a1ade344ae66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 246
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}