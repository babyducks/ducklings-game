{
    "id": "5f20c816-a49d-4357-9e0b-a86568891e29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_menu",
    "eventList": [
        {
            "id": "00e33b16-c4dd-4f12-9af6-9f373b5e94b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f20c816-a49d-4357-9e0b-a86568891e29"
        },
        {
            "id": "f288c0e0-4c6a-44b8-80f3-7bcda3e62d15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f20c816-a49d-4357-9e0b-a86568891e29"
        },
        {
            "id": "07efd6b2-3497-4d8e-9e60-95c078c0c124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5f20c816-a49d-4357-9e0b-a86568891e29"
        },
        {
            "id": "8d77f3e1-486e-49c0-a3f5-5b6b08682e3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "5f20c816-a49d-4357-9e0b-a86568891e29"
        }
    ],
    "maskSpriteId": "38666889-b865-4f2c-a294-b863c8251b3f",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}