{
    "id": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "RunningRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 5,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "397e92ea-36b9-49cf-855a-bb450edf5309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "02abcc79-9331-417f-9a5f-62dcf3eefbbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397e92ea-36b9-49cf-855a-bb450edf5309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ec9bcb-4228-4a37-8587-4751de96464b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397e92ea-36b9-49cf-855a-bb450edf5309",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        },
        {
            "id": "ef4c126f-edc8-4bab-8593-20174be28a4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "92f96b79-ae2f-49dd-b6af-fca35033a14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4c126f-edc8-4bab-8593-20174be28a4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65d785f-2d57-4f6d-a310-db9302fbde07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4c126f-edc8-4bab-8593-20174be28a4c",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        },
        {
            "id": "ff3bf251-9e5a-40f8-9407-3045ef250935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "6f6882b9-b785-46d6-82c8-1a07e6ca16d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3bf251-9e5a-40f8-9407-3045ef250935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c79e2dbf-3066-4163-8bb7-d9bcd2527f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3bf251-9e5a-40f8-9407-3045ef250935",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        },
        {
            "id": "07f00935-766d-4bbc-b974-ad5b2e178b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "8f3904c0-eb7c-46bb-9302-6cc2e5442e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f00935-766d-4bbc-b974-ad5b2e178b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8342460-14bc-459b-b341-78d8e4ba54d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f00935-766d-4bbc-b974-ad5b2e178b72",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        },
        {
            "id": "1dd8df5d-013c-4e4a-8d69-720a0d7db6aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "b94512fe-411e-42c5-a304-61d7ec7d152e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dd8df5d-013c-4e4a-8d69-720a0d7db6aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a09c47-315b-4834-8933-f2cf8f884c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dd8df5d-013c-4e4a-8d69-720a0d7db6aa",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        },
        {
            "id": "54cb7480-307c-4eba-ae9d-b6a11c20fc29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "compositeImage": {
                "id": "a5570c50-a183-45c6-8d8a-20c0b399ee9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54cb7480-307c-4eba-ae9d-b6a11c20fc29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4b4c10-9e9e-407d-bbd0-18962e781e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54cb7480-307c-4eba-ae9d-b6a11c20fc29",
                    "LayerId": "d36439e5-7207-4f8f-bf29-0e750edface7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d36439e5-7207-4f8f-bf29-0e750edface7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1b3d167-95ae-4f84-93aa-88c0655c95e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 23
}