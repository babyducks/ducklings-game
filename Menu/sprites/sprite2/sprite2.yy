{
    "id": "88970a51-2680-4e77-bef5-d800f2801766",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ef9ee94-4df7-4742-b3a2-c5e5efc0ba0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88970a51-2680-4e77-bef5-d800f2801766",
            "compositeImage": {
                "id": "9dae3e0d-e316-4c29-816b-18f6e2c431ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef9ee94-4df7-4742-b3a2-c5e5efc0ba0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "093c9b6d-91f9-4d1d-b7c7-d4615366421c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef9ee94-4df7-4742-b3a2-c5e5efc0ba0d",
                    "LayerId": "d50b3aab-8cfa-417f-8b37-52b41434556d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d50b3aab-8cfa-417f-8b37-52b41434556d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88970a51-2680-4e77-bef5-d800f2801766",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}