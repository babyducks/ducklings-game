{
    "id": "4dfecaee-79f4-4a20-8575-474c90d93c20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec606eb0-dad6-46c3-b098-9fcf83dd5a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dfecaee-79f4-4a20-8575-474c90d93c20",
            "compositeImage": {
                "id": "604183bf-3755-4aa5-a01f-1c5421d132aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec606eb0-dad6-46c3-b098-9fcf83dd5a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2039d9-d64d-4367-87b4-7e99b491ae51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec606eb0-dad6-46c3-b098-9fcf83dd5a00",
                    "LayerId": "8e725933-65bf-4083-b86b-357819b2e01d"
                }
            ]
        }
    ],
    "gridX": 965,
    "gridY": 365,
    "height": 64,
    "layers": [
        {
            "id": "8e725933-65bf-4083-b86b-357819b2e01d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dfecaee-79f4-4a20-8575-474c90d93c20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}