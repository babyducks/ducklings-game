{
    "id": "ca5031bb-2f36-442a-bf75-7f8b66d2c798",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TIleSetSprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 575,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d91745ad-f8da-4a08-93ee-02d8bb6558f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca5031bb-2f36-442a-bf75-7f8b66d2c798",
            "compositeImage": {
                "id": "f329784b-9163-40de-8a89-84683739b13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91745ad-f8da-4a08-93ee-02d8bb6558f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a58841-a074-4c9b-b243-a13dedf81e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91745ad-f8da-4a08-93ee-02d8bb6558f3",
                    "LayerId": "81f3be57-739d-4348-b9c4-1500ad97a3a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 576,
    "layers": [
        {
            "id": "81f3be57-739d-4348-b9c4-1500ad97a3a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca5031bb-2f36-442a-bf75-7f8b66d2c798",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 110,
    "yorig": 181
}