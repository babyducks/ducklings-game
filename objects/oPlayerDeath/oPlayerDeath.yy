{
    "id": "da4ac933-b5eb-4d2c-a1f1-cf9968136b99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayerDeath",
    "eventList": [
        {
            "id": "a52c976e-9e4f-4ae4-9793-62631c3a7d0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da4ac933-b5eb-4d2c-a1f1-cf9968136b99"
        },
        {
            "id": "35738121-6fd0-4fec-b53c-20499400663f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da4ac933-b5eb-4d2c-a1f1-cf9968136b99"
        },
        {
            "id": "bc6916df-62ac-4885-9cc6-7c6494c10c5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "da4ac933-b5eb-4d2c-a1f1-cf9968136b99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "97ad35e0-5392-4b7d-ae02-27427bee5a2b",
    "visible": true
}