{
    "id": "5d44900c-622c-418d-a318-3551427ce545",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 565,
    "bbox_left": 0,
    "bbox_right": 223,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d819af2-89ca-490d-91c5-ba69f5dee580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d44900c-622c-418d-a318-3551427ce545",
            "compositeImage": {
                "id": "99ef0b84-47d8-4c98-a779-caf852636f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d819af2-89ca-490d-91c5-ba69f5dee580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866b4fac-da0d-4211-aad0-b48c01d75ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d819af2-89ca-490d-91c5-ba69f5dee580",
                    "LayerId": "5172022c-1a32-4d88-aebf-8f051f34b09e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 576,
    "layers": [
        {
            "id": "5172022c-1a32-4d88-aebf-8f051f34b09e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d44900c-622c-418d-a318-3551427ce545",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 9,
    "yorig": 46
}