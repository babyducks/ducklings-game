{
    "id": "f70b4c4d-d293-4bc1-8298-6465a2d01c27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlaceHolder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81c3f08e-cbf1-4fca-b4a4-0a06040dabe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70b4c4d-d293-4bc1-8298-6465a2d01c27",
            "compositeImage": {
                "id": "ea9d6350-e217-41c9-9884-e5be6fd184db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81c3f08e-cbf1-4fca-b4a4-0a06040dabe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2bbb7f-c42b-487d-ab6f-eeb868e16bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81c3f08e-cbf1-4fca-b4a4-0a06040dabe6",
                    "LayerId": "ea832f94-4dac-48df-9a42-2a21ec6c3673"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "ea832f94-4dac-48df-9a42-2a21ec6c3673",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f70b4c4d-d293-4bc1-8298-6465a2d01c27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}