{
    "id": "58a18b63-8ea3-4e8d-970b-7de1a90601ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f847cb0-2e5e-4f98-8315-e47da720d45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58a18b63-8ea3-4e8d-970b-7de1a90601ce",
            "compositeImage": {
                "id": "2791d94c-ed37-4528-9417-717a50bf3db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f847cb0-2e5e-4f98-8315-e47da720d45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec32ef5-fc6d-425d-843b-7b019b8152a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f847cb0-2e5e-4f98-8315-e47da720d45e",
                    "LayerId": "d2626154-235e-47e8-b9d9-ff4360c6d53c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d2626154-235e-47e8-b9d9-ff4360c6d53c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58a18b63-8ea3-4e8d-970b-7de1a90601ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}