{
    "id": "03c63557-9c11-4427-9e91-af0336f7affe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sfloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a935196-507e-4cfb-b538-e70cd1473c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c63557-9c11-4427-9e91-af0336f7affe",
            "compositeImage": {
                "id": "83e2872f-dab4-47c4-a82e-ab8ed70d1475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a935196-507e-4cfb-b538-e70cd1473c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642a1e2d-1535-43ad-aff7-85703e55bfc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a935196-507e-4cfb-b538-e70cd1473c94",
                    "LayerId": "87021a5d-0686-474a-bad1-984d004bc4b3"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "87021a5d-0686-474a-bad1-984d004bc4b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c63557-9c11-4427-9e91-af0336f7affe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 17
}