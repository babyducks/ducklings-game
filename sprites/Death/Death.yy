{
    "id": "97ad35e0-5392-4b7d-ae02-27427bee5a2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 8,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef67edf2-4cef-4aa6-89ad-d0ac93cf3127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ad35e0-5392-4b7d-ae02-27427bee5a2b",
            "compositeImage": {
                "id": "1ee44fcb-e77d-46d2-ab91-d7b9bc8497bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef67edf2-4cef-4aa6-89ad-d0ac93cf3127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17e9163-9325-4d2a-8e0a-fcc81c920011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef67edf2-4cef-4aa6-89ad-d0ac93cf3127",
                    "LayerId": "180c7c97-5d72-41a0-bd1e-410baca5fabe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "180c7c97-5d72-41a0-bd1e-410baca5fabe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97ad35e0-5392-4b7d-ae02-27427bee5a2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 20,
    "yorig": 22
}