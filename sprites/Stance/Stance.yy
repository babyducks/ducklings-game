{
    "id": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Stance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 10,
    "bbox_right": 22,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7c52b7c-a1b1-46ec-a5b6-c33e516a74ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
            "compositeImage": {
                "id": "abc3cbb5-4872-453d-8e42-e717ac449c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c52b7c-a1b1-46ec-a5b6-c33e516a74ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4becd8d1-3515-4d81-896f-619c9d709e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c52b7c-a1b1-46ec-a5b6-c33e516a74ad",
                    "LayerId": "085ceb6f-f7dc-4604-a8e3-ded2571f15e6"
                }
            ]
        },
        {
            "id": "8674b3e6-d8ea-47c3-b7cc-587987c906fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
            "compositeImage": {
                "id": "8d5761c2-7960-4e2c-9a07-4840a547a1f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8674b3e6-d8ea-47c3-b7cc-587987c906fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f35a69c8-d520-43cc-a4a9-544f0a4c5057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8674b3e6-d8ea-47c3-b7cc-587987c906fd",
                    "LayerId": "085ceb6f-f7dc-4604-a8e3-ded2571f15e6"
                }
            ]
        },
        {
            "id": "b6e895e6-e952-4cbc-bc59-2b17d3247289",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
            "compositeImage": {
                "id": "9c076b97-9079-4a85-8d61-57cbdd17ae46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e895e6-e952-4cbc-bc59-2b17d3247289",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c07bf8d-e473-4753-90da-d4f104f3cc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e895e6-e952-4cbc-bc59-2b17d3247289",
                    "LayerId": "085ceb6f-f7dc-4604-a8e3-ded2571f15e6"
                }
            ]
        },
        {
            "id": "6d8af769-a6d5-4a95-bb14-57e133fac58e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
            "compositeImage": {
                "id": "142f9809-d981-4ebc-b991-867015f728c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8af769-a6d5-4a95-bb14-57e133fac58e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f39aca-5360-4c6d-8661-069a7839e469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8af769-a6d5-4a95-bb14-57e133fac58e",
                    "LayerId": "085ceb6f-f7dc-4604-a8e3-ded2571f15e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "085ceb6f-f7dc-4604-a8e3-ded2571f15e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 23
}