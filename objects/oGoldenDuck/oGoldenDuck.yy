{
    "id": "11601b2d-3b20-4304-bca6-01b387b9a6c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGoldenDuck",
    "eventList": [
        {
            "id": "df24e8ab-7102-427d-9bb3-909437671fa0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "06d4e32c-79f0-4136-9038-7b08c6507ec3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "11601b2d-3b20-4304-bca6-01b387b9a6c4"
        },
        {
            "id": "e66ca5db-a979-4cdb-8bec-03f49cbc1232",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "11601b2d-3b20-4304-bca6-01b387b9a6c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "67f2a4bc-cfb1-4b6e-b3ef-817eb558a519",
    "visible": true
}