/// @description Insert description here
// You can write your code in this editor
key_left = keyboard_check(vk_left);
key_right = keyboard_check(vk_right);
key_jump = keyboard_check_pressed(vk_up);


var move = key_right - key_left;

hsp = move * walksp; 

vsp = vsp + grv;

if (place_meeting(x, y+1,ofloor)) and (key_jump)
{
	vsp = -4;
}

if (place_meeting(x+hsp,y,oWall)) 
{
	while (!place_meeting(x+sign(hsp),y,oWall))
	{
		x = x + sign(hsp);
	}
	hsp = 0;
}
x = x + hsp;

if (place_meeting(x,y+vsp,ofloor)) 
{
	while (!place_meeting(x+sign(vsp),y,ofloor))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

if (place_meeting(x,y+vsp,oWall)) 
{
	while (!place_meeting(x,y+sign(vsp),oWall))
	{
		y = y + sign(vsp);
	}
	vsp = 0;
}

y = y + vsp;

if (place_meeting(x, y, oWall)) or (place_meeting(x, y, ofloor))
{
	for (var i = 0; i < 1000; i++)
	{
		//right
		if (!place_meeting(x + i, y, oWall)) or (!place_meeting(x+i, y, ofloor))
		{
			x += i;
			break;
		}
		//left
		if (!place_meeting(x - i, y, oWall)) or (!place_meeting(x-i, y, ofloor))
		{
			x -= i;
			break;
		}
		//up 
		if (!place_meeting(x, y - i, oWall)) or (!place_meeting(x, y - i, ofloor))
		{
			y -= i;
			break;
		}
		//down
		if (!place_meeting(x , y + i, oWall)) or (!place_meeting(x, y + i, ofloor))
		{
			y += i;
			break;
		}
		//top right
		if (!place_meeting(x + i, y - i, oWall)) or (!place_meeting(x + i, y - i, ofloor))
		{
			x += i;
			y -= i;
			break;
		}
		//top left 
		if (!place_meeting(x - i, y - i, oWall)) or (!place_meeting(x - i, y - i, ofloor))
		{
			x -= i;
			y -= i;
			break;
		}
		//bottom right
		if (!place_meeting(x + i, y + i, oWall)) or (!place_meeting(x + i, y + i, ofloor))
		{
			x += i;
			y += i;
			break;
		}
		//bottom left
		if (!place_meeting(x - i, y + i, oWall)) or (!place_meeting(x - i, y + i, ofloor))
		{
			x -= i;
			y += i;
			break;
		}
	}
}

//animation 
//falling (not yet set)
if (!place_meeting(x, y+1, ofloor)) 
{
	sprite_index = Jumping;
	image_speed = 0;
	if (sign(vsp) > 0) image_index = 2; else image_index =0;
}
else if (!place_meeting(x, y+1, ofloor)) 
{
	sprite_index = Jumping;
	image_speed = 0;
	if (sign(vsp) > 0) image_index = 2; else image_index =0;
}
else 
{
	image_speed = 1;
	if (hsp == 0)
	{
		sprite_index = Stance;
	}
	else if (key_right)
	{
		sprite_index = Running;
		image_xscale = 2;
	}
	else if (key_left)
	{
		sprite_index = Running;
		image_xscale = -2;
	}
}