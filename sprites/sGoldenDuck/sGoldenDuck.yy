{
    "id": "67f2a4bc-cfb1-4b6e-b3ef-817eb558a519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGoldenDuck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00047b92-e337-4e53-b188-7f7f657a9baf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67f2a4bc-cfb1-4b6e-b3ef-817eb558a519",
            "compositeImage": {
                "id": "70b83906-0acb-48d3-8d65-62cf4581d0ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00047b92-e337-4e53-b188-7f7f657a9baf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db4a295a-1ba8-4851-a1b8-097dcc7a6716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00047b92-e337-4e53-b188-7f7f657a9baf",
                    "LayerId": "8badf7e8-6e54-4c47-8fde-69cad32a91eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8badf7e8-6e54-4c47-8fde-69cad32a91eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67f2a4bc-cfb1-4b6e-b3ef-817eb558a519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 24,
    "yorig": 24
}