{
    "id": "8d11e8f8-dc89-4d43-a5c7-52705920ba25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMainMenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 482,
    "bbox_left": 0,
    "bbox_right": 644,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bc3d757-4dd0-444e-9f00-682e4b486014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d11e8f8-dc89-4d43-a5c7-52705920ba25",
            "compositeImage": {
                "id": "cda210bc-7365-47a0-8096-2cfe950fb6af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc3d757-4dd0-444e-9f00-682e4b486014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df80f2a2-3404-4dd9-b5a0-7d9216d2d4bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc3d757-4dd0-444e-9f00-682e4b486014",
                    "LayerId": "254c805c-2338-47bf-9a8b-96a7b85efc5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 483,
    "layers": [
        {
            "id": "254c805c-2338-47bf-9a8b-96a7b85efc5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d11e8f8-dc89-4d43-a5c7-52705920ba25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 645,
    "xorig": 0,
    "yorig": 0
}