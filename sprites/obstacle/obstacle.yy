{
    "id": "3a0654b3-3f7c-43e8-8814-ba07272efc7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "obstacle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e570e933-1e9b-495e-9037-24ca260e9eab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a0654b3-3f7c-43e8-8814-ba07272efc7b",
            "compositeImage": {
                "id": "9d103d21-b82e-40cc-9e00-afd1b46e064d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e570e933-1e9b-495e-9037-24ca260e9eab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6fb7ba1-9e0b-4e28-a8a6-eaf332d53c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e570e933-1e9b-495e-9037-24ca260e9eab",
                    "LayerId": "106976d0-92c4-41ee-9ef9-fdf551b36768"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "106976d0-92c4-41ee-9ef9-fdf551b36768",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a0654b3-3f7c-43e8-8814-ba07272efc7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}