//Update destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y-96;
}
//Update object position
x = xTo;
y = yTo;

x = clamp(x,view_w_half,room_width-view_w_half);
y = clamp(y,view_h_half,room_height-view_h_half);

//update camera view
camera_set_view_pos(camera,x-view_w_half,y-view_h_half);