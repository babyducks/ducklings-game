{
    "id": "06d4e32c-79f0-4136-9038-7b08c6507ec3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Player",
    "eventList": [
        {
            "id": "be30721c-346d-4fed-9926-f9b90f7ef66d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "06d4e32c-79f0-4136-9038-7b08c6507ec3"
        },
        {
            "id": "95649b85-d0c1-4ea2-8e3a-a7eb67a8a892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "06d4e32c-79f0-4136-9038-7b08c6507ec3"
        },
        {
            "id": "19ce3c46-3861-45fb-ab0e-a10a13489321",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3f7452ec-e731-463e-9637-8be5fe72a8e8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "06d4e32c-79f0-4136-9038-7b08c6507ec3"
        },
        {
            "id": "23b46464-72ba-41cd-abf5-0b5c02690492",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 5,
            "m_owner": "06d4e32c-79f0-4136-9038-7b08c6507ec3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "7f40a466-327e-4d54-a189-f44039e177b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bd837cf2-2ebb-402e-b5b6-49282d0d2b07",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "31a1dd05-f8fa-45be-8da1-8973f5c6a158",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "75a65504-95eb-485a-9f14-21748738bcc7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "6e71630c-3cc1-4c5a-a343-5a6d07d5d0ae",
    "visible": true
}