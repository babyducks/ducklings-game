{
    "id": "972e0349-e9ff-4d4a-8d38-04014e016c12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnding",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 482,
    "bbox_left": 0,
    "bbox_right": 644,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85e28930-88de-45a1-a046-a18455674728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "972e0349-e9ff-4d4a-8d38-04014e016c12",
            "compositeImage": {
                "id": "aef4b165-c80b-4489-8c79-0ed1fd61e75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85e28930-88de-45a1-a046-a18455674728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462508b8-39ce-4b66-93d3-4ae5cf9bd945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85e28930-88de-45a1-a046-a18455674728",
                    "LayerId": "2c134314-99c7-48b4-8b68-f4f1a955c38f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 483,
    "layers": [
        {
            "id": "2c134314-99c7-48b4-8b68-f4f1a955c38f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "972e0349-e9ff-4d4a-8d38-04014e016c12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 645,
    "xorig": 0,
    "yorig": 0
}