{
    "id": "d7852f55-05b2-4599-ada3-d142f82ded59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "swall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd5bfd69-9c49-4398-8da4-0cf2fe554e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7852f55-05b2-4599-ada3-d142f82ded59",
            "compositeImage": {
                "id": "5155c71c-0a48-49ba-bcd9-8666013dddf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd5bfd69-9c49-4398-8da4-0cf2fe554e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650e98c0-97b6-4724-8cfa-43228eb0bc5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd5bfd69-9c49-4398-8da4-0cf2fe554e8e",
                    "LayerId": "f7ba4d24-1b2f-4119-9e31-41256ad7210b"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 64,
    "layers": [
        {
            "id": "f7ba4d24-1b2f-4119-9e31-41256ad7210b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7852f55-05b2-4599-ada3-d142f82ded59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}